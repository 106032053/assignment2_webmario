# Software Studio 2020 Spring Assignment 2
## Notice
* Replace all [xxxx] to your answer

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|10%|Y|
|Complete Game Process|5%|Y|
|Basic Rules|45%|Y|
|Animations|10%|Y|
|Sound Effects|10%|Y|
|UI|10%|Y|

## Website Detail Description :
以下為遊戲流程，一開始進入匯到登入畫面，玩家須登入或註冊帳號後才可以遊玩:
p.s因為沒有利用preload，所以第一次開啟時，點選一些按鈕都要等一下子才會轉換視窗
(大概5~10秒左右)
1. 登入畫面

![](https://i.imgur.com/rhm4Zsb.jpg)

帳號連結firebase，帳號格式須符合email的形式，若帳號已存在或不符合形式皆會跳出警告視窗，若登入帳號或密碼錯誤也會跳出警告視窗。

2. Menu

![](https://i.imgur.com/D3F43M4.jpg)

登入成功後便會自動轉向到menu，玩家可以選擇要去的介面或者是登出帳號。

3. START

![](https://i.imgur.com/ScnsUFq.jpg)

點選START按鈕後，會進入關卡選擇的頁面，總共有三種遊戲選擇，分別為PVP對戰的BATTLE模式以及玩家闖關的正常模式(分為EASY以及HARD兩張地圖)。

4. EASY地圖

![](https://i.imgur.com/piEXdmk.jpg)

重力普通的地圖。

5. HARD地圖

![](https://i.imgur.com/k6ej7ic.jpg)

重力較小的水中地圖。

6. BATTLE模式

![](https://i.imgur.com/5tQk6K1.jpg)

PVP模式。

7. SETTING

![](https://i.imgur.com/PTMI9Iq.jpg)

這邊可以對BGM進行設定，一共有四種選擇(三種BGM以及靜音)，玩家只須點選BGM按鈕進行切換即可。

8. CONTROL

![](https://i.imgur.com/5ad3BxR.jpg)

這邊可以看操作說明。

# Basic Components Description : 
### World map :
EASY與HARD地圖的背景及camera皆會針對玩家所在的位置進行更新。
1. EASY

![](https://i.imgur.com/xZ89JKK.jpg)

關卡開始

![](https://i.imgur.com/pgExcoY.jpg)

關卡中間


![](https://i.imgur.com/RzhuY1I.jpg)

到達終點

![](https://i.imgur.com/eotVN7L.jpg)

過關

![](https://i.imgur.com/ZyjjUr8.jpg)

失敗

2. HARD

![](https://i.imgur.com/FYhHWnO.jpg)

關卡開始

![](https://i.imgur.com/TQqOTH0.jpg)

關卡中間

![](https://i.imgur.com/6SaYgUt.jpg)

轉場，走進水管會轉換場地

![](https://i.imgur.com/xK8fNQt.jpg)

進水管後會轉換到另一個場地
p.s. 在這個場地已經算過關了，這個場地只是給玩家享受碰旗子的樂趣，因此分數以及時間會在碰到水管時便上傳到雲端

![](https://i.imgur.com/Tej96Sv.jpg)

到達終點

![](https://i.imgur.com/4Vq4Ixw.jpg)

過關

![](https://i.imgur.com/eSIAxOs.jpg)

失敗

3. BATTLE

![](https://i.imgur.com/H0MxdtD.jpg)
頭上I的是player 1，II的是player 2，玩家用金幣擊中對方兩次便取得勝利。

p.s. 金幣發射的冷卻時間為0.3 seconds

![](https://i.imgur.com/8ntfYuL.jpg)

以此圖為例，player 1擊中player 2兩次，因此player 1獲勝，失敗的一方會有類似跪地的動畫。

![](https://i.imgur.com/F9rzgdo.jpg)

接著會切換至玩家的獲勝畫面，會依據獲勝的玩家而顯示不同的獲勝訊息，如此圖為player 1獲勝。

### Player :
這一部分用圖片不好贅述，因此僅用文字闡述功能。玩家失去一條命後皆會在起始位置重生。
1. EASY
玩家可以在地圖中自由地走動以及跳躍，且掉到地圖外以及碰到敵人(除了用踩的)皆會失去一條命。玩家踩到敵人會對敵人造成傷害並且會自動地向上彈一小段距離(跟一般的馬利歐一樣)，玩家也可以用頭撞地圖中的問號方塊。
p.s 按S會有蹲下的行為，只是目前沒有用處。

2. HARD
玩家可以在地圖中自由的游泳，按W會往上游一小段距離(冷卻時間為0.3 seconds)，按住S則會快速往下墜(方便對敵人進行攻擊)，從上方踩到敵人一樣會自動往上彈一小段距離，晚皆也一樣可以用頭撞地圖中的問號方塊。

### Enemies : 
1. Goomba
以下以EASY地圖為例子:

![](https://i.imgur.com/Ya8Meqd.png)

正常的樣子

![](https://i.imgur.com/XqYrkaX.jpg)

被玩家踩一次之後會扁掉，此時若直接碰觸依舊會失去一條命，要再從上面踩他一次才會死掉。

![](https://i.imgur.com/GfUWiAf.jpg)

若被龜殼撞到則會彈到空中呈現生氣的樣子，落地後即死掉，但要注意在落地前若玩家不幸碰到的話一樣畫失去一條命。

2. Turtle
以下以EASY地圖為例子:

![](https://i.imgur.com/Sz9Dkvk.jpg)

正常的樣子

![](https://i.imgur.com/zoib0ky.jpg)

被玩家踩到後便會變成龜殼，可以再採一次龜殼即可使龜殼動起來，龜殼移動時撞到的怪物都會直接死掉，撞到玩家也會使玩家少一條命。

### Question Blocks :
1. 綠蘑菇

![](https://i.imgur.com/28kZIcO.jpg)

撞方塊前，方塊會有問號的動畫。

![](https://i.imgur.com/lJUAvsV.jpg)

撞擊方塊後會出現綠蘑菇，此時方塊的特效會消失
p.s. 吃了綠蘑菇會使玩家速度上升，但若碰到敵人或掉到外面皆會失去能力(會變回原本的速度)。

2. 紅蘑菇

![](https://i.imgur.com/2xgqWTL.jpg)

撞方塊前，方塊會有問號的動畫。

![](https://i.imgur.com/UdmoKTP.jpg)

撞擊方塊後會出現紅蘑菇，此時方塊的特效會消失

![](https://i.imgur.com/NaOS3Bb.jpg)

吃了紅蘑菇會使玩家變大隻(變胖)，但若碰到敵人或掉到外面皆會失去能力(會變回原本的大小)。

3. 無敵星星

![](https://i.imgur.com/nTAg85g.jpg)

撞方塊前，方塊會有問號的動畫。

![](https://i.imgur.com/SkVa8kL.jpg)

撞擊方塊後會出現無敵星星，此時方塊的特效會消失
p.s. 吃了無敵星星會使玩家有七秒的無敵時間以及速度上升(此時碰到怪物即可使怪物死掉)，且吃到星星的當下會切換背景音樂(附檔的bgm3，因此若原本玩家在播bgm3會比較聽不出來)，當無敵時間結束後便會切會成另一個背景音樂(附檔的bgm2)。

4. 金幣

![](https://i.imgur.com/AH5DVnl.jpg)

撞方塊前，方塊會有發光的動畫。

![](https://i.imgur.com/nylUX5U.jpg)

撞擊方塊後會出現金幣，且玩家分數會加一分。

![](https://i.imgur.com/txoqODe.jpg)

當方塊的金幣沒了之後，方塊的特效會消失。

5. 勝利旗子

![](https://i.imgur.com/j26iLKt.jpg)

放在關卡的終點

![](https://i.imgur.com/XKaKPGS.jpg)

玩家碰到後便會出現跳舞的動畫，且經過3秒後會切換至勝利畫面。

### Animations :
1. EASY
走路會有動畫，但截圖看不太出來，因此沒有截圖。

![](https://i.imgur.com/Mux1blf.jpg)

在地板不動時，會有不同於走路的動畫。

![](https://i.imgur.com/BPFZAD9.jpg)

按W跳躍時也會有相對應的動畫。

![](https://i.imgur.com/1yT223y.jpg)

按S會有蹲下的動畫。

2. HARD
在地上走路會有動畫，但截圖依樣也看不太出來，因此沒有截圖。

![](https://i.imgur.com/WZjwlZS.jpg)

在地板不動時，會有不同於走路的動畫。

![](https://i.imgur.com/vavNr17.jpg)

不論是按下W往上游(類似跳躍)，或著是在水中移動(不在地板上時)，皆會有游泳的動畫。

![](https://i.imgur.com/zwAhTPH.jpg)

在水中按下S會往下加速且有屁股向下的動畫。

3. Goomba
走路時會有動畫，被踩到一次會變扁，被龜殼撞到也會有死掉的動畫。

4. Tutle
烏龜走路會有動畫，龜殼移動時也會有移動的動畫，被龜殼撞到也會有死掉的動畫。

5. 綠蘑菇
方塊本身會有問號動畫，出現的綠蘑菇則是會慢慢移動。

6. 紅蘑菇
方塊本身會有問號動畫，出現的紅蘑菇則是會慢慢移動。

7. 無敵星星
方塊本身會有問號動畫，出現的無敵星星會有閃亮發光的動畫。

8. 金幣
方塊本身會有發光動畫，出現的金幣則是會上彈一小段距離後再下墜。

9. 勝利失敗

![](https://i.imgur.com/KMfzc1n.jpg)

獲勝馬力歐會有跳舞的動畫

![](https://i.imgur.com/kHjImUv.jpg)

勝利畫面馬力歐有動畫

![](https://i.imgur.com/cZfjak8.jpg)

失敗畫面馬力歐會有動畫。

10. BATTLE模式

![](https://i.imgur.com/4eHO1Ai.jpg)

BATTLE被打敗的一方會有動畫。

![](https://i.imgur.com/AaTcK5T.jpg)

BATTLE勝利畫面馬力歐會有動畫。

11. Menu

![](https://i.imgur.com/NQolRvl.jpg)

Menu畫面的馬利歐與烏龜皆有走路動畫。



### Sound effects :
1. 馬利歐

跳躍

碰到敵人或掉到外面

吃到道具

失去道具能力

碰到勝利旗子

2. Goomba

被踩到

死掉

3. Turtle

被踩到

死掉

4. Turtle Shield

被踩到

撞到牆壁反彈

撞到怪物

5. 綠蘑菇

出現時音效

6. 紅蘑菇

出現時音效

7. 無敵星星

出現時音效

被吃掉會切換背景音樂(切成附檔的bgm3)

8. 金幣

會有金幣出現的音效

9. 勝利失敗

皆會有相對應的音效

10. 轉場音效

HARD最後碰到水管裡面會有轉場音效。

11. 所有按鈕

按下去會有音效

### UI :

![](https://i.imgur.com/fgeTNBg.jpg)

上圖紅框由左至右分別為，剩餘生命;計時器;分數
1. 剩餘生命

一開始設定為3(代表除當前生命之外，還有三條命)，若玩家碰到怪物(在沒有道具能力下)或掉出地圖外，皆會減一，若小於0則會切換至失敗畫面，通關後會上傳剩餘生命至firebase。

2. 計時器

紀錄玩家的遊戲時間，通關後會上傳至firebase。

3. 分數

紀錄玩家得分，通關後會上傳至firebase。
p.s.

金幣一個一分

把扁掉的Goomba踩死得一分

把turtle踩成龜殼得一分

龜殼擊殺任何怪物皆得一分

![](https://i.imgur.com/PlJwvna.jpg)

上傳至雲端的玩家數據會以不同地圖來做分類，如圖可以看到會顯示玩家帳號, 剩餘生命, 得分以及通關時間。
# Bonus Functions Description : 
### BATTLE MODE
可以使玩家利用互丟金幣來對戰(非網路連線)

### 水中特殊重力處理
水中重力要調比較低，跳躍移動速度也要特別處理，且對著地的判斷與否要做出游泳或走路的動畫，以及多加一層半頭明藍色的圖在畫面最上層(跟背景及camera一樣跟著玩家移動)，使畫面更有在水裡的感覺。
